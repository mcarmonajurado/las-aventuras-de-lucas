#ifndef ELEMENT_H
#define ELEMENT_H

#include "Character.h"

#define TIMER_PATICLE 2.0

enum ESTADO_ELEMENT {
    COGIENDO, SIN_COGER, COGIDO
};

class Element : public Character
{
public:
    Element ( Ogre::SceneManager* sceneMgr, OgreBulletDynamics::DynamicsWorld* world, const string& nombre,
              const Ogre::Vector3& posicion );
    virtual ~Element();
    Element(const Element& personaje);
    Element& operator=(const Element& rhs);

    ESTADO_ELEMENT getState() const { return _state; }
    void changeAnimation(string nameAnimation);
    void coger();
    void setVisible ( const bool visible );
    void update ( double timeSinceLastFrame, std::vector<Character*> vCharacteres);
    double _timerParticle;
    void flotar ();

protected:
    void copy ( const Element& personaje );

private:
    Ogre::ParticleSystem* _particle;
    Ogre::SceneNode* _particleNode;
    ESTADO_ELEMENT _state;
    SoundFXPtr _soundFreeFX;
};

#endif // ELEMENT_H
