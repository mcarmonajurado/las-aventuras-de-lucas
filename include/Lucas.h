#ifndef LUCAS_H
#define LUCAS_H

#include <Character.h>

class Lucas : public Character
{
public:
    Lucas ( Ogre::SceneManager* sceneMgr, OgreBulletDynamics::DynamicsWorld* world, const string& nombre,
            const Ogre::Vector3& posicion );
    virtual ~Lucas();
    Lucas(const Lucas& personaje);
    Lucas& operator=(const Lucas& rhs);

protected:
    void copy ( const Lucas& personaje );

};

#endif // LUCAS_H
