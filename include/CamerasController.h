#ifndef CAMERAS_CONTROLLER_H
#define CAMERAS_CONTROLLER_H

#include <OGRE/Ogre.h>
#include "Lucas.h"

class CamerasController {
public:
    CamerasController(Ogre::SceneManager* sceneMgr, Ogre::Camera* camera, Ogre::Camera* camaraMapa, Lucas* lucas);
    void update(double timeSinceLastFrame);
    ~CamerasController();
private:
    // Referencias
    Ogre::Camera* _camera;
    Ogre::Camera* _camaraMapa;
    Lucas* _lucas;
    Ogre::SceneNode* _cameraNode;
    Ogre::SceneNode* _camaraMapaNode;
    Ogre::SceneNode* _targetNode;
};

#endif  // CAMERAS_CONTROLLER_H
