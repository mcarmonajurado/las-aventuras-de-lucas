/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <OGRE/Ogre.h>
#include <OIS/OIS.h>

#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>

#include "GameState.h"
#include "Character.h"
#include "Lucas.h"
#include "Fader.h"
#include "CameraMapTextureListener.h"
#include "CamerasController.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
public:
    PlayState () {}

    void enter ();
    void exit ();
    void pause ();
    void resume ();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);
    bool frameRenderingQueued (const Ogre::FrameEvent& evt);

    void buildGUI();

    // Heredados de Ogre::Singleton.
    static PlayState& getSingleton ();
    static PlayState* getSingletonPtr ();

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;

    Ogre::Camera* _camaraMapa;
    Ogre::OverlayManager* _overlayMgr;
    std::vector<Fader*> _vFader;
    CameraMapTextureListener* _textureListener;
    Ogre::RenderTexture* _rtex;

    bool _exitGame;

private:
    OgreBulletDynamics::DynamicsWorld * _world;
    OgreBulletCollisions::DebugDrawer * _debugDrawer;

    double _tiempo;
    double _tiempoInicial;
    double _mejorTiempo;
    bool _empieza_a_contar;
    bool _jueboAcabado;
    bool _muerto;
    int _numElementos;
    float _velocidadLucas;

    Lucas* _lucas;
    std::deque<Enemy*> _enemigos;
    std::vector<Character*> _vPersonajes;
    std::vector<Element*> _vElementos;
    CamerasController* _camerasController;
    Ogre::StaticGeometry* _staticGeometry;

    void gestionCamaras();
    void gestionSonido();
    void gestionOverlay();
    void crearMapa();
    void crearEscenario();

    string getTime ( double time );

    void createInitialWorld();
    void createScene();
    void update(double timeSinceLastFrame);
    OgreBulletDynamics::RigidBody* insertarElementoEscena (std::string nombreElemento, std::string mesh);
    void crearPlano ( Ogre::SceneManager* sceneMgr,
                      OgreBulletDynamics::DynamicsWorld* world,
                      Ogre::StaticGeometry *staticGeometry,
                      std::string namePlane,
                      std::string nameMaterial,
                      float width,
                      float height,
                      Ogre::Vector3 upVector,
                      Ogre::Vector3 normal,
                      const Ogre::Vector3& initial_pos );

    Element* detectarColisionElementos(OgreBulletDynamics::DynamicsWorld* world,
                                Character* lucas,
                                std::vector<Element*> personajes);
    Character* detectarColision(OgreBulletDynamics::DynamicsWorld* world,
                                Character* lucas,
                                std::deque<Enemy*> personajes);

};

#endif
