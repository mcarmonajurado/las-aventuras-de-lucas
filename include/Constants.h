#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

// Archivos mesh para cargar
#define CUBE_MESH "Cube"
//#define LUCAS_MESH "Lucas"
#define LUCAS_MESH "Cuerpo"
#define ENEMIGO_MESH "Enemigo"
#define ELEMENTO_MESH "Elemento"
#define EXTENSION_MESH ".mesh"

#define FICHERO_RECORDS "records.txt"

// Animacion del objeto mesh
#define INICIAR_ANIMACION "Andar"
#define PARAR_ANIMACION "Parar"

#define POSICIONES_JUEGO "posiciones.xml"

// Nombre personajes
#define LUCAS_NOMBRE "Lucas"
#define ENEMIGO_NOMBRE "Enemigo"
#define HUESO_NOMBRE "Hueso"
#define ELEMENTO_NOMBRE "Elemento"

#define PLATAFORMA "button"

#define SCENE_MANAGER "SceneManager"

#define CAMARA_PRINCIPAL "IntroCamera"
#define CAMARA_MAPA "CamaraMapa"

#define TEXTURA_MAPA "RttTextura_Map"
#define MATERIAL_MAPA "RttMaterial_Map"

#define SONG_INTRO "Carlos_Estella_-_After_Tomorrow.mp3"
#define ESTADOS "112071__cheesepuff__piano-remix.mp3"
#define PRINCIPAL "Sardonica_-_Through_to_Reality__Original_.mp3"

#define LADRIDO "155309__jace__dog-double-bark.wav"
#define ELEMENTO_COGIDO "27568__suonho__memorymoon-space-blaster-plays.wav"
#define ELEMENTO_COGIDO_2 "27568__suonho__memorymoon-space-blaster-plays2.wav"


#endif
 
