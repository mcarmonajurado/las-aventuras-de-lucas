#ifndef _RECORDS_H_
#define _RECORDS_H_

#include <list>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <string>
#include <string.h>
#include <time.h>

using namespace std;

class Records {

private:
    static Records* msSingleton;
    // hacemos privado el constructor para tener un singleton
    Records();
    // Constructor de copia
    Records ( Records& source );

    virtual ~Records();

    void Copy ( Records &source );

    // Compara una nueva línea de récord a insertar con los récords ya insertados
    bool esMejorRecord ( int acertijos, int tSegundos, std::string record );
    // Devuelve en las variables fecha y hora la fecha y hora
    void getFechaHora ( std::string &fecha, std::string &hora );

    //Lista de récords
    std::list<std::string> _records;

public:
    static Records& getSingleton();
    static Records* getSingletonPtr();

    Records operator= ( Records& source );

    // Número de elementos de la lista de récords
    unsigned int getSize() { return _records.size(); }

    // devuelte el récord en la posición pasada
    std::string getRecord ( unsigned int index );

    void anadirRecord ( int acertijos, int tSegundos );
    unsigned int grabarRecords();
    unsigned int leerRecords();
};

#endif
