#ifndef CHARACTER_H
#define CHARACTER_H

#include <stdlib.h>

#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/OgreBulletCollisionsRay.h>

#include "SoundFXManager.h"

using namespace std;


#define VELOCIDAD_ANDAR 10.0

#define VELOCIDAD 2.0
#define VELOCIDAD_ANIMACION 2.0

#define FACTOR_VELOCIDAD_RAPIDA 1.5

enum TIPO_PERSONAJE {
    LUCAS, ENEMIGO, ELEMENTO
};

enum ESTADO_PERSONAJE {
    ACTIVO, INACTIVO
};

class Character
{

private:

protected:
    void copy ( const Character& personaje );

    string _nombrePersonaje;
    TIPO_PERSONAJE _tipoPersonaje;
    ESTADO_PERSONAJE _estadoPersonaje;
    // coordenadas inciales del personaje (X, Y, Z)
    Ogre::Vector3 _posicion;

    Ogre::SceneManager* _sceneMgr;
    Ogre::Entity* _entity;
    Ogre::SceneNode* _node;
    Ogre::AnimationState* _animacionActual;
    Ogre::Entity* _entityPuntoMapa;
    Ogre::SceneNode* _nodePuntoMapa;


    OgreBulletDynamics::DynamicsWorld* _world;

    bool _visible;

    //Sonidos del personaje
    //TODO

    double _distanciaAOtroPersonaje;
    Ogre::Vector3 _vDistanciaAOtroPersonaje;

public:
    OgreBulletDynamics::RigidBody* _rigidBody;
    Character ( Ogre::SceneManager* sceneMgr, OgreBulletDynamics::DynamicsWorld* world,
                const string& nombre, const Ogre::Vector3& posicion, TIPO_PERSONAJE tipo, string animacion);
    virtual ~Character();
    Character ( const Character& personaje );
    Character& operator= ( const Character& rhs );

    // movimientos
    void andar ( float velocidad = VELOCIDAD );
    void pararMovimiento();
    void girarIzq();
    void girarDer();
    void moverAngulo ( const Ogre::Radian& angle );

    // nombre del personaje principal
    string getNombrePersonaje() const { return _nombrePersonaje; }
    void setNombrePersonaje ( const string& nombre ) { _nombrePersonaje = nombre; }

    // tipo de personaje
    TIPO_PERSONAJE getTipoPersonaje() const { return _tipoPersonaje; }
    ESTADO_PERSONAJE getEstadoPersonaje() const { return _estadoPersonaje; }

    // visibilidad del persojane
    bool isVisible() const { return _visible; }
    virtual void setVisible ( const bool visible );

    // situa el personaje con coordenadas ( X, Y, Z). Posición inicial
    void setInitial_Pos ( const Ogre::Vector3& posicion ) { _posicion = posicion; }
    const Ogre::Vector3& getInitial_Pos() const { return _posicion; }

    Ogre::SceneNode* getSceneNode() const { return _node; }
    OgreBulletDynamics::RigidBody* getRigidBody() const { return _rigidBody; }
    void setRigidBody(OgreBulletDynamics::RigidBody* rigidBody) { _rigidBody = rigidBody; }
    void setNode(Ogre::SceneNode* node) { _node = node; }
    Ogre::Entity* getEntity() const { return _entity; }
    Ogre::Entity* getEntityPuntoMapa() const { return _entityPuntoMapa; }

    virtual void update ( double timeSinceLastFrame, std::vector<Character*> vCharacteres );
    virtual void cambiarAnimacion ( const string& nameAnimation );

    virtual void showPuntoMapa ( bool show );

    // posición actual del personaje
    const Ogre::Vector3& getPosition();


    void insertarEnEscena ( Ogre::SceneManager* sceneMgr,
                            OgreBulletDynamics::DynamicsWorld* world,
                            string name_mesh,
                            string name_element,
                            const Ogre::Vector3& initial_pos,
                            Ogre::Entity** entity,
                            Ogre::SceneNode** node,
                            OgreBulletDynamics::RigidBody** rigidTrack,
                            bool visible,
                            string initAnimation );

    double distanciaAPersonaje(const Ogre::Vector3& position);
};
#endif // CHARACTER_H
