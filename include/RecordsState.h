#ifndef RECORDSSTATE_H
#define RECORDSSTATE_H

#include <OGRE/Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

class RecordsState : public Ogre::Singleton<RecordsState>, public GameState
{
public:
    RecordsState() {}

    void enter ();
    void exit ();
    void pause ();
    void resume ();

    void keyPressed (const OIS::KeyEvent &e);
    void keyReleased (const OIS::KeyEvent &e);

    void mouseMoved (const OIS::MouseEvent &e);
    void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    bool frameStarted (const Ogre::FrameEvent& evt);
    bool frameEnded (const Ogre::FrameEvent& evt);
    bool frameRenderingQueued (const Ogre::FrameEvent& evt);

    // GUI
    void buildGUI();
    void buttonHit(OgreBites::Button* button);
    void yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit);

    // Heredados de Ogre::Singleton.
    static RecordsState& getSingleton ();
    static RecordsState* getSingletonPtr ();

protected:
    Ogre::Root* _root;
    Ogre::SceneManager* _sceneMgr;
    Ogre::Viewport* _viewport;
    Ogre::Camera* _camera;

    bool _exitGame;
    string _records;
    Ogre::OverlayManager* _overlayMgr;

private:
    void recuperaRecords();
    void showRecords(bool visible);
    void gestionOverlay();
};

#endif // RECORDSSTATE_H
