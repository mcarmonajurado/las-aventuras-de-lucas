#ifndef ENEMY_H
#define ENEMY_H

#include "Character.h"
#include "Lucas.h"

class Enemy : public Character
{
public:
    Enemy ( Ogre::SceneManager* sceneMgr, OgreBulletDynamics::DynamicsWorld* world, const string& nombre,
            const Ogre::Vector3& posicion );
    virtual ~Enemy();
    Enemy(const Enemy& personaje);
    Enemy& operator=(const Enemy& rhs);
    void updateEnemy ( double timeSinceLastFrame, std::vector<Character*> vCharacteres, Lucas* lucas );
    void perseguir ( float velocidad, Lucas* lucas );

protected:
    void copy ( const Enemy& personaje );

private:
    SoundFXPtr _soundFreeFX;

};

#endif // ENEMY_H
