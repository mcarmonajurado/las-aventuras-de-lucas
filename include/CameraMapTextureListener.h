#ifndef CameraMapTextureListener_H
#define CameraMapTextureListener_H

#include <OGRE/Ogre.h>
#include <vector>
#include "Character.h"

using namespace std;
using namespace Ogre;

class CameraMapTextureListener : public RenderTargetListener
{
private:
    std::vector<Character*> _vCharacteres;
    Ogre::SceneManager* _sceneMgr;

public:
    CameraMapTextureListener ( Ogre::SceneManager* sceneMgr, std::vector<Character*> vCharacteres);
    ~CameraMapTextureListener();
    virtual void preRenderTargetUpdate ( const RenderTargetEvent& evt );
    virtual void postRenderTargetUpdate ( const RenderTargetEvent& evt );

};

#endif // CameraMapTextureListener_H
