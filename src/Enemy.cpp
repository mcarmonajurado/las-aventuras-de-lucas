#include "Enemy.h"
#include "Constants.h"

#define DISTANCIA_VISION_CERCA 8.0
#define DISTANCIA_VISION_LEJOS 11.0

#define MARGEN_GIRO_1 135
#define MARGEN_GIRO_2 45
#define MARGEN_GIRO_3 0.1

Enemy::Enemy( Ogre::SceneManager* sceneMgr,
              OgreBulletDynamics::DynamicsWorld* world,
              const string& nombre,
              const Ogre::Vector3& posicion ) : Character ( sceneMgr,
                                                            world,
                                                            nombre,
                                                            posicion,
                                                            ENEMIGO,
                                                            PARAR_ANIMACION )
{
    Ogre::LogManager::getSingletonPtr()->logMessage("Constructor de enemigos");

    _soundFreeFX = SoundFXManager::getSingleton().load(LADRIDO);
}

Enemy::~Enemy()
{
}

Enemy::Enemy(const Enemy& personaje) : Character ( personaje )
{
    copy ( personaje );
}

Enemy& Enemy::operator=(const Enemy& rhs)
{
    if (this != &rhs)
        copy ( rhs );

    return *this;
}

void Enemy::copy ( const Enemy& personaje )
{
    Character::copy ( personaje );
}

void Enemy::updateEnemy ( double timeSinceLastFrame, std::vector<Character*>   vPersonajes, Lucas* lucas)
{
    Character::update(timeSinceLastFrame, vPersonajes);
    distanciaAPersonaje(lucas->getPosition());
    if(_distanciaAOtroPersonaje < DISTANCIA_VISION_CERCA && _distanciaAOtroPersonaje >-DISTANCIA_VISION_CERCA){
        if(!_soundFreeFX->isPlaying())
        _soundFreeFX->play();
        perseguir ( VELOCIDAD_ANDAR , lucas);
//        Ogre::LogManager::getSingletonPtr()->logMessage("te persigo");
    }else if (_distanciaAOtroPersonaje > DISTANCIA_VISION_LEJOS || _distanciaAOtroPersonaje <- DISTANCIA_VISION_LEJOS){
        Character::pararMovimiento();
//        Ogre::LogManager::getSingletonPtr()->logMessage("no te persigo");
    }
}

void Enemy::perseguir ( float velocidad , Lucas* lucas)
{
    Ogre::Vector3 dif = lucas->getPosition() - getPosition();

    Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z;

//    moverAngulo ( orientacion.getRotationTo(dif).getYaw() );

    if ( orientacion.getRotationTo(dif).getYaw().valueDegrees() > 0 ){
        if ( orientacion.getRotationTo(dif).getYaw().valueDegrees() > MARGEN_GIRO_1 ){
            moverAngulo ( orientacion.getRotationTo(dif).getYaw()/3 );
        }else if ( orientacion.getRotationTo(dif).getYaw().valueDegrees() > MARGEN_GIRO_2 ){
            moverAngulo ( orientacion.getRotationTo(dif).getYaw()/2 );
        }else if ( orientacion.getRotationTo(dif).getYaw().valueDegrees() > MARGEN_GIRO_3 ){
            moverAngulo ( orientacion.getRotationTo(dif).getYaw() );
        }
    }
    else if ( orientacion.getRotationTo(dif).getYaw().valueDegrees() < -MARGEN_GIRO_1 ){
        moverAngulo ( orientacion.getRotationTo(dif).getYaw()/3 );
    }else if ( orientacion.getRotationTo(dif).getYaw().valueDegrees() < -MARGEN_GIRO_2 ){
        moverAngulo ( orientacion.getRotationTo(dif).getYaw()/2 );
    }else if ( orientacion.getRotationTo(dif).getYaw().valueDegrees() < -MARGEN_GIRO_3 ){
        moverAngulo ( orientacion.getRotationTo(dif).getYaw() );
    }

    andar(velocidad);
}
