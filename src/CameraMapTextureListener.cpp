#include "CameraMapTextureListener.h"

CameraMapTextureListener::CameraMapTextureListener ( Ogre::SceneManager* sceneMgr, std::vector<Character*> vCharacteres)
{
//    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("CameraMapTextureListener::CameraMapTextureListener");
//    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
  _vCharacteres = vCharacteres;
  _sceneMgr = sceneMgr;
}

CameraMapTextureListener::~CameraMapTextureListener()
{
  _vCharacteres.clear();
  _sceneMgr = NULL;
}

void CameraMapTextureListener::preRenderTargetUpdate ( const RenderTargetEvent& evt )
  {
//    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("CameraMapTextureListener::preRenderTargetUpdate");
//    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    // Desactivamos las sombras en el mapa
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_NONE);

	// Recorremos todos los personajes y los mostramos como cubos en el mapa
    Character *character;
    for (unsigned int i = 0; i < _vCharacteres.size(); i++) {
        character = _vCharacteres[i];

        if (character->isVisible()) {
            character->showPuntoMapa(true);
            character->getEntity()->setCastShadows(false);
        }
    }
  }

void CameraMapTextureListener::postRenderTargetUpdate ( const RenderTargetEvent& evt )
  {
//    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("CameraMapTextureListener::postRenderTargetUpdate");
//    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
	// Activamos las sombras
	_sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

	// Recorremos todos los personajes y los mostramos con su entidad original
    Character *character;
    for (unsigned int i = 0; i < _vCharacteres.size(); i++) {
		character = _vCharacteres[i];

        if (character->isVisible()) {
            character->showPuntoMapa(false);
			character->getEntity()->setCastShadows(true);
		}
	}
  }
