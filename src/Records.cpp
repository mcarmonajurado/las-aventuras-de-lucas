#include "Records.h"
#include "Constants.h"

Records* Records::msSingleton = NULL;

const char *FILENAME = FICHERO_RECORDS;

Records::Records()
{
    _records.clear();
}

Records::~Records()
{
    _records.clear();
}

Records& Records::getSingleton()
{
    if ( NULL == msSingleton )
        msSingleton = new Records;

    return (*msSingleton);
}

Records* Records::getSingletonPtr()
{
    if ( NULL == msSingleton )
        msSingleton = new Records();

    return (msSingleton);
}

void Records::Copy ( Records &source )
{
    _records.clear();

    for ( unsigned int i = 0; i < source.getSize(); i++ )
        _records.push_back ( source.getRecord ( i ) );
}

Records::Records ( Records& source )
{
    Copy ( source );
}

Records Records::operator= ( Records& source )
{
    Copy ( source );
    return *this;
}

void Records::anadirRecord ( int acertijos, int tSegundos )
{
    char linea[100];
    std::string fecha;
    std::string hora;

    //obtengo la fecha y hora del sistema
    getFechaHora ( fecha, hora );

    //nos creamos la línea de récord a insertar
    sprintf ( linea, "%d|%d|%s|%s",  acertijos, tSegundos, fecha.c_str(), hora.c_str() );

    //buscamos en la lista de récords si hay más récords con los que comparar el que vamos a insertar
    std::list<std::string>::iterator it = _records.begin();
    if ( _records.size() > 0 )
    {
        std::list<std::string>::iterator itEnd = _records.end();
        for ( ; it != itEnd; ++it )
        {
            /* comparamos la línea de récord a insertar con cada una de las ya presentes,
             * si encontramos un récord inferior salimos del bucle y añadimos el récord*/
            if ( esMejorRecord (  acertijos, tSegundos, *it ) )
                break;
        }
    }

    _records.insert ( it, std::string(linea) );

}

bool Records::esMejorRecord ( int  acertijos, int tSegundos, std::string record )
{
    bool esMejor = false;

    int tSegundosAComparar = 0;
    int acertijosAComparar = 0;
    char basura[100];

    sscanf ( record.c_str(), "%d|%d|%s", & acertijosAComparar, & tSegundosAComparar, basura );

    if ( (  tSegundos < tSegundosAComparar ) ||
         (  tSegundos ==  tSegundosAComparar && acertijos > acertijosAComparar ) )
    {
        esMejor = true;
    }

    return esMejor;
}

std::string Records::getRecord ( unsigned int index )
{
    std::string record = "";

    if ( index >= 0 && index < _records.size() )
    {
        std::list<std::string>::iterator it = _records.begin();
        std::advance ( it, index );
        record = *it;
    }
    else{
        clog << "Índice fuera de rango" << endl;
    }

    return record;
}

unsigned int Records::grabarRecords()
{
    ofstream os ( FILENAME );

    if ( !os.is_open() )
    {
        clog << "Error en método grabarRecords() al abrir el fichero " << FILENAME << endl;
        return 1;
    }
    else
    {
        std::list<std::string>::iterator it, itEnd;

        for ( it = _records.begin(), itEnd = _records.end(); it != itEnd; ++it )
        {
            os << *it << endl;
        }

        os.close();
    }
    return 0;
}

unsigned int Records::leerRecords()
{
    ifstream is ( FILENAME );
    std::string linea = "";

    _records.clear();

    if ( !is.is_open() )
    {
        clog << "Error en método leerRecords() al abrir el fichero " << FILENAME << endl;
        return 1;
    }
    else
    {
        while ( !is.eof() )
        {
            is >> linea;
            if (!is.eof())
            {
                _records.push_back ( linea );
            }
        }

        is.close();
    }
    return 0;
}

void Records::getFechaHora ( std::string &fecha, std::string &hora )
{
    time_t now;
    struct tm *ts;
    char buf[100];

    /* Obtener la hora actual */
    now = time(0);
    ts = localtime(&now);

    sprintf ( buf, "%04d-%02d-%02d", ts->tm_year + 1900, ts->tm_mon + 1, ts->tm_mday);

    fecha = buf;//recuperamos la fecha y la asignamos al parámetro de i/O

    sprintf ( buf, "%02d:%02d", ts->tm_hour, ts->tm_min );

    hora = buf;//recuperamos la hora y la asignamos al parámetro de i/O
}
