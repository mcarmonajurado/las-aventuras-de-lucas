#include <OGRE/Ogre.h>

#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>

#include "GameManager.h"
#include "GameState.h"

template<> GameManager* Ogre::Singleton<GameManager>::msSingleton = 0;
const char* GameManager::RESOURCES = "resources.cfg";
const char* GameManager::FICHERO_RECORDS = "records.txt";
const char* GameManager::TITLE = "Juego";

GameManager::GameManager ()
{
    _root = 0;

    //sonido
    initSDL();
}

GameManager::~GameManager ()
{
    while (!_states.empty()) {
        _states.top()->exit();
        _states.pop();
    }

    if(_pTrayMgr)
        delete _pTrayMgr;

//    if(_pTrackManager)
//        delete _pTrackManager;

//    if(_pSoundFXManager)
//        delete _pSoundFXManager;

    if(_inputMgr)
        delete _inputMgr;

    if (_root)
        delete _root;
}

void
GameManager::start
(GameState* state)
{
    // Creación del objeto Ogre::Root.
    _root = new Ogre::Root();

    loadResources();

    // Sonido
    _pTrackManager = new TrackManager;
    _pSoundFXManager = new SoundFXManager;

    if (!configure())
        return;

    _inputMgr = new InputManager;
    _inputMgr->initialise(_renderWindow);

    // Registro como key y mouse listener...
    _inputMgr->addKeyListener(this, "GameManager");
    _inputMgr->addMouseListener(this, "GameManager");

    // El GameManager es un FrameListener.
    _root->addFrameListener(this);

    // Inicialización del GUI
    _pTrayMgr = new OgreBites::SdkTrayManager("TrayMgr", _root->getAutoCreatedWindow(), _inputMgr->getMouse(), 0);

    // Transición al estado inicial.
    changeState(state);

    // Bucle de rendering.
    _root->startRendering();
}

void
GameManager::changeState
(GameState* state)
{
    // Limpieza del estado actual.
    if (!_states.empty()) {
        // exit() sobre el último estado.
        _states.top()->exit();
        // Elimina el último estado.
        _states.pop();
    }

    // Transición al nuevo estado.
    _states.push(state);
    // enter() sobre el nuevo estado.
    _states.top()->enter();
}

void
GameManager::cleanAndChangeState
(GameState* state)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("cleanAndChangeState");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    // Limpieza del estado actual.
    while (!_states.empty()) {
        // exit() sobre el último estado.
        _states.top()->exit();
        // Elimina el último estado.
        _states.pop();
    }
    // Transición al nuevo estado.
    _states.push(state);
    // enter() sobre el nuevo estado.
    _states.top()->enter();
}

void
GameManager::pushState
(GameState* state)
{
    // Pausa del estado actual.
    if (!_states.empty())
        _states.top()->pause();

    // Transición al nuevo estado.
    _states.push(state);
    // enter() sobre el nuevo estado.
    _states.top()->enter();
}

void
GameManager::popState ()
{
    // Limpieza del estado actual.
    if (!_states.empty()) {
        _states.top()->exit();
        _states.pop();
    }

    // Vuelta al estado anterior.
    if (!_states.empty())
        _states.top()->resume();
}

void
GameManager::loadResources ()
{
    Ogre::ConfigFile cf;
    cf.load(RESOURCES);

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typestr = i->first;    datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation
                    (datastr, typestr, sectionstr);
        }
    }
}

bool
GameManager::configure ()
{
    if (!_root->restoreConfig()) {
        if (!_root->showConfigDialog()) {
            return false;
        }
    }

    _renderWindow = _root->initialise(true, TITLE);

    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    return true;
}

GameManager*
GameManager::getSingletonPtr ()
{
    return msSingleton;
}

GameManager&
GameManager::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

// Las siguientes funciones miembro delegan
// el evento en el estado actual.
bool
GameManager::frameStarted
(const Ogre::FrameEvent& evt)
{
    _inputMgr->capture();
    return _states.top()->frameStarted(evt);
}

bool
GameManager::frameEnded
(const Ogre::FrameEvent& evt)
{
    return _states.top()->frameEnded(evt);
}

bool
GameManager::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    return _states.top()->frameRenderingQueued(evt);
}

bool
GameManager::keyPressed
(const OIS::KeyEvent &e)
{
    _states.top()->keyPressed(e);
    return true;
}

bool
GameManager::keyReleased
(const OIS::KeyEvent &e)
{
    _states.top()->keyReleased(e);
    return true;
}

bool
GameManager::mouseMoved
(const OIS::MouseEvent &e)
{

    int x = e.state.X.abs;
    int y = e.state.Y.abs;
    int xmax = e.state.width;
    int ymax = e.state.height;
    if(x<0 || y<0 || x>xmax || y>ymax)
        _pTrayMgr->hideCursor();
    else
        _pTrayMgr->showCursor();

    _states.top()->mouseMoved(e);
    return true;
}

bool
GameManager::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    _states.top()->mousePressed(e, id);
    return true;
}

bool
GameManager::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    _states.top()->mouseReleased(e, id);
    return true;
}

bool GameManager::initSDL ()
{
    // Inicializando SDL...
    if (SDL_Init(SDL_INIT_AUDIO) < 0)
        return false;
    // Llamar a  SDL_Quit al terminar.
    atexit(SDL_Quit);

    // Inicializando SDL mixer...
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0)
        return false;

    // Llamar a Mix_CloseAudio al terminar.
    atexit(Mix_CloseAudio);

    return true;
}
