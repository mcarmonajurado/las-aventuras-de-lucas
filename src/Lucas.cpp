#include "Lucas.h"
#include "Constants.h"

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>

Lucas::Lucas( Ogre::SceneManager* sceneMgr,
              OgreBulletDynamics::DynamicsWorld* world,
              const string& nombre,
              const Ogre::Vector3& posicion ) : Character ( sceneMgr,
                                                            world,
                                                            nombre,
                                                            posicion,
                                                            LUCAS,
                                                            PARAR_ANIMACION )
{
    Ogre::LogManager::getSingletonPtr()->logMessage("Constructor de Lucas");

}

Lucas::~Lucas()
{
}

Lucas::Lucas(const Lucas& personaje) : Character ( personaje )
{
    copy ( personaje );
}

Lucas& Lucas::operator=(const Lucas& rhs)
{
    if (this != &rhs)
        copy ( rhs );

    return *this;
}

void Lucas::copy ( const Lucas& personaje )
{
    Character::copy ( personaje );
}
