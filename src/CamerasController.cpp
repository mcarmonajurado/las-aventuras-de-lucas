#include "CamerasController.h"

#define ALTURA_CAMARA 0.01f
#define ALTURA_CAMARA_MINIMAP 120.0f
#define DISTANCIA_A_CAMARA_Z 0.1f
#define DISTANCIA_A_CAMARA_Z_MINIMAP 1.0f

CamerasController::CamerasController(Ogre::SceneManager* sceneMgr, Ogre::Camera* camera, 
                                     Ogre::Camera* camaraMapa, Lucas* lucas):
    _camera(camera),_camaraMapa(camaraMapa), _lucas(lucas)
{
    if(NULL != _lucas){
        _cameraNode = sceneMgr->getRootSceneNode()->createChildSceneNode("Camera" + lucas->getNombrePersonaje());
        _camaraMapaNode = sceneMgr->getRootSceneNode()->createChildSceneNode("camaraMapa" + lucas->getNombrePersonaje());
        _targetNode = sceneMgr->getRootSceneNode()->createChildSceneNode("Camera_target");

        // Hacemos que el nodo de la cámara siempre esté mirando al nodo objetivo.
        //del juego
        _cameraNode->setAutoTracking(true, _targetNode);
        _cameraNode->setFixedYawAxis(true);
        //de la mini cámara
        _camaraMapaNode->setAutoTracking(true, _targetNode);
        _camaraMapaNode->setFixedYawAxis(true);

        // Valores fijos de las distancias para reenderizar
        //del juego
        _camera->setNearClipDistance(ALTURA_CAMARA / 2);
        _camera->setFarClipDistance(ALTURA_CAMARA * 2);
        _cameraNode->attachObject(_camera);
        //de la mini cámara
        _camaraMapa->setNearClipDistance(ALTURA_CAMARA_MINIMAP / 2);
        _camaraMapa->setFarClipDistance(ALTURA_CAMARA_MINIMAP * 2);
        _camaraMapaNode->attachObject(_camaraMapa);
    }
} 

void CamerasController::update(double timeSinceLastFrame) {
    if(NULL != _lucas){
        Ogre::Vector3 posLucas = _lucas->getRigidBody()->getCenterOfMassPosition();
        posLucas.y = 1.0;

        // Situamos las cámaras
        Ogre::Vector3 cameraPosition = posLucas + Ogre::Vector3::UNIT_Z * DISTANCIA_A_CAMARA_Z
                + Ogre::Vector3::UNIT_Y * ALTURA_CAMARA;
        Ogre::Vector3 _displacement = (cameraPosition - _cameraNode->getPosition()) * timeSinceLastFrame;
        _cameraNode->translate(_displacement);

        cameraPosition = posLucas + Ogre::Vector3::UNIT_Z * DISTANCIA_A_CAMARA_Z_MINIMAP
                + Ogre::Vector3::UNIT_Y * ALTURA_CAMARA_MINIMAP;
        _displacement = (cameraPosition - _camaraMapaNode->getPosition()) * timeSinceLastFrame;
        _camaraMapaNode->translate(_displacement);

        // Situamos el objetivo
        _displacement = (posLucas - _targetNode->getPosition()) * timeSinceLastFrame;
        _targetNode->translate(_displacement);
    }
}

CamerasController::~CamerasController() {
    if(_cameraNode){
        _cameraNode->detachAllObjects();
        if(_cameraNode->getCreator())
            _cameraNode->getCreator()->destroySceneNode(_cameraNode);
    }
    if(_camaraMapaNode){
        _camaraMapaNode->detachAllObjects();
        if(_camaraMapaNode->getCreator())
            _camaraMapaNode->getCreator()->destroySceneNode(_camaraMapaNode);
    }
    if(_targetNode){
        _targetNode->detachAllObjects();
        if(_targetNode->getCreator())
            _targetNode->getCreator()->destroySceneNode(_targetNode);
    }

    _camera = NULL;
    _camaraMapa = NULL;
    _lucas = NULL;

    _cameraNode = NULL;
    _camaraMapaNode = NULL;
    _targetNode = NULL;
}
