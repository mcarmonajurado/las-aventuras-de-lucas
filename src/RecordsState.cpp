#include "RecordsState.h"

#include "IntroState.h"
#include "Constants.h"
#include "Records.h"

template<> RecordsState* Ogre::Singleton<RecordsState>::msSingleton = 0;

void
RecordsState::enter ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");
    Ogre::LogManager::getSingletonPtr()->logMessage("RecordsState::enter");
    Ogre::LogManager::getSingletonPtr()->logMessage("===================");

    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera(CAMARA_PRINCIPAL);
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    // Nuevo background colour.
    _viewport->setBackgroundColour(Ogre::ColourValue(0.7, 0.0, 0.2));

    _exitGame = false;

    buildGUI();

    // Carga del sonido.
    TrackPtr mainTrack = GameManager::getSingletonPtr()->getTrackManager()->load(ESTADOS);
    GameManager::getSingletonPtr()->setTrackPtr(mainTrack);

    // Reproducción del track principal...
    mainTrack->play();

    gestionOverlay();
    showRecords(true);
}

void
RecordsState::exit ()
{
    showRecords(false);

    if(!GameManager::getSingletonPtr()->getTrackPtr().isNull())
        GameManager::getSingletonPtr()->getTrackPtr()->stop();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();

    if(GameManager::getSingletonPtr()->getTrayManager()){
        GameManager::getSingletonPtr()->getTrayManager()->clearAllTrays();
        GameManager::getSingletonPtr()->getTrayManager()->destroyAllWidgets();
        GameManager::getSingletonPtr()->getTrayManager()->setListener(0);
    }
}

void
RecordsState::pause ()
{
}

void
RecordsState::resume ()
{
    buildGUI();
}

bool
RecordsState::frameStarted
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);
    return true;
}

bool
RecordsState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
RecordsState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);

    if (_exitGame)
        return false;

    Ogre::OverlayElement * oe = _overlayMgr->getOverlayElement("RecordsValues");
    oe->setCaption ( _records );

    return true;
}

void
RecordsState::keyPressed
(const OIS::KeyEvent &e) {
    // Tecla p --> Estado anterior.
    if (e.key == OIS::KC_A) {
        Ogre::LogManager::getSingletonPtr()->logMessage("Estado anterior");
        popState();
    }
}

void
RecordsState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
RecordsState::mouseMoved
(const OIS::MouseEvent &e)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseMove(e);
}

void
RecordsState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseDown(e, id);
}

void
RecordsState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseUp(e, id);
}

RecordsState*
RecordsState::getSingletonPtr ()
{
    return msSingleton;
}

RecordsState&
RecordsState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void RecordsState::buildGUI()
{
    OgreBites::SdkTrayManager* trayMgr = GameManager::getSingletonPtr()->getTrayManager();
    trayMgr->setListener(this);
    trayMgr->destroyAllWidgets();
    trayMgr->showCursor();
    trayMgr->createLabel(OgreBites::TL_TOP, "CreditsLbl", "Récords", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "BackToMenuBtn", "Volver al Menu", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "ExitBtn", "Salir", 250);

    Ogre::FontManager::getSingleton().getByName("SdkTrays/Caption")->load();
    Ogre::FontManager::getSingleton().getByName("SdkTrays/Value")->load();
}

void RecordsState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "ExitBtn")
        GameManager::getSingletonPtr()->getTrayManager()->showYesNoDialog("¿Estás seguro?", "¿Quieres salir realmente?");
    else if(button->getName() == "BackToMenuBtn")
        popState();
}

void RecordsState::yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit)
{
    if(yesHit == true)
        _exitGame = true;
    else
        GameManager::getSingletonPtr()->getTrayManager()->closeDialog();
}

void RecordsState::gestionOverlay()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("RecordsState::gestionOverlay");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    _overlayMgr = Ogre::OverlayManager::getSingletonPtr();
}

void RecordsState::recuperaRecords()
{
    Records::getSingleton().leerRecords();

    string msg = "Posición -- Tiempo -- Fecha \n\n";
    int segundos = 0;
    int acertijos = 0;
    char fecha[100];
    char hora[100];
    char new_hora[100];
    int minutos = 0;

    if ( Records::getSingleton().getSize() > 0 )
    {
        char cad[100];

        for ( unsigned int i = 0; i < Records::getSingleton().getSize(); i++ )
        {
            //Entre la fecha y la hora no detecta el separador '|' así que plan B, es decir, cojo posiciones de bytes
            //y luego formateo la hora en new_hora que ya no tendría dentro el caracter '|'
            sscanf ( Records::getSingleton().getRecord(i).c_str(), "%d|%d|%10s%6s", &acertijos, &segundos, fecha, hora );
            memcpy ( new_hora, hora+1, strlen(hora)-1);
            new_hora[5]=0;
            minutos = segundos / 60;
            segundos = segundos % 60;
            sprintf ( cad, "     %d -- %02d:%02d -- %s a las %s \n", i+1, minutos, segundos, fecha, new_hora );
            msg += string ( cad );
        }
    }
    else
        msg = "No hay records";

    _records = msg;
}

void RecordsState::showRecords (bool visible)
{
    Ogre::Overlay *over = _overlayMgr->getByName ( "Records_Menu" );
    if(visible)
        over->show();
    else
        over->hide();

    recuperaRecords();
}
