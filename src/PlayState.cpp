﻿#include "PlayState.h"
#include "PauseState.h"
#include "GameOverState.h"

#include "WinningState.h"

#include "Constants.h"
#include "Lucas.h"
#include "Fader.h"
#include "Records.h"

#include <vector>

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h>

#include <OgreBullet/Dynamics/OgreBulletDynamicsWorld.h>
#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/Debug/OgreBulletCollisionsDebugDrawer.h>

#include <OgreBullet/Dynamics/Constraints/OgreBulletDynamicsRaycastVehicle.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsConvexHullShape.h>
#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager(SCENE_MANAGER);
    _camera = _sceneMgr->getCamera(CAMARA_PRINCIPAL);
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    // Nuevo background colour.
    //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
    //_viewport->setBackgroundColour(Ogre::ColourValue(0.f, 0.f, 0.f, 1.0f));


    // variables del juego
    _exitGame = false;
    _jueboAcabado = false;
    _muerto = false;
    _tiempoInicial = 120;//valor por defecto
    _numElementos = 1;

    _lucas = NULL;
    _vPersonajes.clear();
    _enemigos.clear();
    _vElementos.clear();
    _velocidadLucas = VELOCIDAD_ANDAR;

    gestionOverlay();

    gestionCamaras();

    buildGUI();

    gestionSonido();

    createInitialWorld();

    createScene();

    for ( unsigned int i = 0; i < _vFader.size(); i++ )
        if (_vFader[i]) _vFader[i]->startFadeOut(4.0);

    //ajustamos el tiempo
    _tiempoInicial = _tiempoInicial + _tiempo;
}

void
PlayState::exit ()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::exit");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    if(!GameManager::getSingletonPtr()->getTrackPtr().isNull())
        GameManager::getSingletonPtr()->getTrackPtr()->stop();

    for ( unsigned int i = 0; i < _vFader.size(); i++ )
        if (_vFader[i]) delete _vFader[i];
    _vFader.clear();

    std::vector<Character *>::iterator itCharacter = _vPersonajes.begin();
    while ( _vPersonajes.end() != itCharacter )
    {
        if ( *itCharacter )
            delete *itCharacter;
        ++itCharacter;
    }
    _vPersonajes.clear();
    _vElementos.clear();
    _enemigos.clear();

    if ( _camerasController )
        delete _camerasController;

    //    delete _lucas;
    if ( _textureListener )
    {
        _rtex->removeListener ( _textureListener );
        delete _textureListener;
    }
    _rtex->removeAllViewports();

    if ( _camaraMapa )
    {
        _sceneMgr->destroyCamera ( _camaraMapa );
    }

    Ogre::TextureManager::getSingleton().remove(MATERIAL_MAPA);
    Ogre::MaterialManager::getSingleton().remove(TEXTURA_MAPA);

    delete _world;

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    //_overlayMgr->destroyAll();
    _overlayMgr->getByName("GUI_Game")->hide();

    if(GameManager::getSingletonPtr()->getTrayManager()){
        GameManager::getSingletonPtr()->getTrayManager()->clearAllTrays();
        GameManager::getSingletonPtr()->getTrayManager()->destroyAllWidgets();
        GameManager::getSingletonPtr()->getTrayManager()->setListener(0);
    }
}

void
PlayState::pause()
{
}

void
PlayState::resume()
{
    // Se restaura el background colour.
    //_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));

    buildGUI();
    GameManager::getSingletonPtr()->getTrackPtr()->play();
    //mainTrack->play();
}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);
    return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
PlayState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);

    update(evt.timeSinceLastEvent);
    if (_exitGame)
        return false;

    return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
    switch(e.key)
    {
    case OIS::KC_ESCAPE:
        //_exitGame = true;
        pushState(PauseState::getSingletonPtr());
        break;
    case OIS::KC_P:// Tecla p --> PauseState.
        pushState(PauseState::getSingletonPtr());
        break;
    case OIS::KC_SPACE:// resetea el juego
        //changeState(PlayState::getSingletonPtr());
        cleanAndChangeState(PlayState::getSingletonPtr());
        break;
    case OIS::KC_M:// quita o pone el mapa
        if(_overlayMgr->getOverlayElement("Panel_Mapa_Border_Game")->isVisible())
            _overlayMgr->getOverlayElement("Panel_Mapa_Border_Game")->hide();
        else
            _overlayMgr->getOverlayElement("Panel_Mapa_Border_Game")->show();
        break;
    default:
        break;
    }

    if(!_jueboAcabado){
        // Movemos a Lucas
        switch(e.key)
        {
        //        case OIS::KC_RIGHT:
        //             _lucas->girarDer();
        //            break;
        //        case OIS::KC_LEFT:
        //             _lucas->girarIzq();
        //            break;
        //        case OIS::KC_DOWN:
        //              _lucas->andar( - _velocidadLucas );
        //            break;
        //        case OIS::KC_UP:
        //            _lucas->andar ( _velocidadLucas );
        //            break;
        case OIS::KC_R:
            _velocidadLucas = VELOCIDAD_ANDAR * FACTOR_VELOCIDAD_RAPIDA;
            break;
        default:
            break;
        }
    }
}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
    if (e.key == OIS::KC_UP || e.key == OIS::KC_DOWN || e.key == OIS::KC_LEFT || e.key == OIS::KC_RIGHT || e.key == OIS::KC_X ) {
        _lucas->pararMovimiento();
    }

    if ( e.key == OIS::KC_R )
    {
        _velocidadLucas = VELOCIDAD_ANDAR;
    }

}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseMove(e);
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseDown(e, id);
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseUp(e, id);
}

PlayState*
PlayState::getSingletonPtr ()
{
    return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::buildGUI()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::buildGUI");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    OgreBites::SdkTrayManager* trayMgr = GameManager::getSingletonPtr()->getTrayManager();
    trayMgr->setListener(this);
    trayMgr->destroyAllWidgets();
    trayMgr->showCursor();
    //trayMgr->createLabel(OgreBites::TL_TOP, "PauseLbl", "PlayState", 250);

    Ogre::FontManager::getSingleton().getByName("SdkTrays/Caption")->load();
    Ogre::FontManager::getSingleton().getByName("SdkTrays/Value")->load();
}

void PlayState::gestionSonido()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::gestionSonido");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    // Carga del sonido.
    TrackPtr mainTrack = GameManager::getSingletonPtr()->getTrackManager()->load(PRINCIPAL);
    GameManager::getSingletonPtr()->setTrackPtr(mainTrack);

    // Reproducción del track principal.
    mainTrack->play();
    //TODO
}

void PlayState::gestionCamaras()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::gestionCamaras");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    // colocación de la cámara
    _camera->setPosition(Ogre::Vector3 ( 0.0f, 20.0f, 40.0f ) );
    _camera->lookAt(Ogre::Vector3 ( 0.0f, 13.0f, 0.0f ) );
    _camera->setNearClipDistance(0.1f);
    _camera->setFarClipDistance(10000);

    //    _camera->setNearClipDistance(1);
    //    _viewport->setCamera(_camera);
    //    _camera->setAspectRatio(Ogre::Real(_viewport->getActualWidth()) / Ogre::Real(_viewport->getActualHeight()));

    // Creamos la camara del mapa
    try {
        _camaraMapa = _sceneMgr->getCamera(CAMARA_MAPA);
    } catch (Ogre::Exception &e) {
        Ogre::LogManager::getSingletonPtr()->logMessage(e.getDescription());
        _camaraMapa = _sceneMgr->createCamera ( CAMARA_MAPA );
    }
    //TODO
}

void PlayState::gestionOverlay()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::gestionOverlay");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    _overlayMgr = Ogre::OverlayManager::getSingletonPtr();

    // Overlays con fundidos de negro al color del overlay
    Fader* fader = NULL;

    fader = new Fader ( "GUI_Game", "Game/Tiempo", NULL );
    _vFader.push_back ( fader );

    fader = new Fader ( "GUI_Game", "Game/Elementos", NULL );
    _vFader.push_back ( fader );

    fader = new Fader ( "GUI_Game", "Game/Mapa", NULL );
    _vFader.push_back ( fader );
    //TODO
}

void PlayState::crearMapa() {
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::crearMapa");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    // Creamos la textura donde vamos a meter el mapa que va visualizarse a partir de la camara
    Ogre::TexturePtr _rtt = Ogre::TextureManager::getSingleton().createManual (
                TEXTURA_MAPA, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                Ogre::TEX_TYPE_2D, 256, 256, 0, Ogre::PF_A8R8G8B8, Ogre::TU_RENDERTARGET );

    _rtex = _rtt->getBuffer()->getRenderTarget();
    // Vinculamos la camara con la textura
    _rtex->addViewport ( _camaraMapa );
    _rtex->getViewport(0)->setClearEveryFrame ( true );
    _rtex->getViewport(0)->setBackgroundColour ( Ogre::ColourValue::Black );
    _rtex->getViewport(0)->setOverlaysEnabled ( false );
    _rtex->setAutoUpdated(true);
    // Creamos el material al que le vamos a asginar la textura
    Ogre::MaterialPtr mPtr = Ogre::MaterialManager::getSingleton().create ( MATERIAL_MAPA, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
    Ogre::Technique* matTechnique = mPtr->createTechnique();
    matTechnique->createPass();
    mPtr->getTechnique(0)->getPass(0)->setLightingEnabled(true);
    mPtr->getTechnique(0)->getPass(0)->setDiffuse(0.9,0.9,0.9,1);
    mPtr->getTechnique(0)->getPass(0)->setSelfIllumination(0.4,0.4,0.4);
    // Le asignamos la textura que hemos creado
    mPtr->getTechnique(0)->getPass(0)->createTextureUnitState(TEXTURA_MAPA);

    // Le vinculamos el listener a la textura
    _textureListener = new CameraMapTextureListener (_sceneMgr ,_vPersonajes );
    _rtex->addListener ( _textureListener );

    // Asignamos el material al Overlay en el que mostramos el mapa
    Ogre::OverlayElement *elem = _overlayMgr->getOverlayElement("Panel_Mapa");
    elem->setMaterialName ( MATERIAL_MAPA);

    //TODO
}

void PlayState::createScene()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::createScene");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    // Luz de la escena
    Ogre::Light* luz = _sceneMgr->createLight("Luz");
    luz->setType(Ogre::Light::LT_POINT);
    luz->setPosition(75,75,75);
    luz->setSpecularColour(1, 1, 1);
    luz->setDiffuseColour(1, 1, 1);

}

void PlayState::createInitialWorld()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::createInitialWorld");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    // Creacion del modulo de debug visual de Bullet
    //    _debugDrawer = new OgreBulletCollisions::DebugDrawer();
    //    _debugDrawer->setDrawWireframe(true);
    //    Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->createChildSceneNode ( "debugNode", Ogre::Vector3::ZERO );
    //    node->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer));

    // Creacion del mundo (definicion de los limites y la gravedad)
    Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
                Ogre::Vector3 (-10000, -10000, -10000),
                Ogre::Vector3 (10000,  10000,  10000));
    Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

    _world = new OgreBulletDynamics::DynamicsWorld ( _sceneMgr, worldBounds, gravity);
    //_world->setDebugDrawer (_debugDrawer);

    // Activamos las sombras
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    _sceneMgr->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5) );

    _sceneMgr->setShadowTextureCount(2);
    _sceneMgr->setShadowTextureSize(512);

    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.9, 0.9, 0.9));
    //TODO

    // Crear paredes
    crearEscenario();

    //Cargamos todos los elementos de la escena

    //    insertarElementoEscena(std::string("Cuerpo"));
    //    _sceneMgr->getSceneNode("Cuerpo")->setScale(_sceneMgr->getSceneNode("Cuerpo")->getScale()*5);
    //    _sceneMgr->getSceneNode("Cuerpo")->setPosition(0,50,0);

    DotSceneLoader* pDotSceneLoader = new DotSceneLoader();
    pDotSceneLoader->_lucas = _lucas;
    pDotSceneLoader->_vPersonajes = _vPersonajes;
    pDotSceneLoader->_vEnemigos = _enemigos;
    pDotSceneLoader->_vElementos = _vElementos;
    pDotSceneLoader->_world = _world;
    pDotSceneLoader->_tiempoInicial = _tiempoInicial;
    pDotSceneLoader->parseDotSceneCustom(POSICIONES_JUEGO, "General", _sceneMgr, _sceneMgr->getRootSceneNode(),"", _world);
    _lucas = pDotSceneLoader->_lucas;
    _vPersonajes = pDotSceneLoader->_vPersonajes;
    _vElementos = pDotSceneLoader->_vElementos;
    _enemigos = pDotSceneLoader->_vEnemigos;
    _world = pDotSceneLoader->_world;
    _tiempoInicial = pDotSceneLoader->_tiempoInicial;
    _numElementos = _vElementos.size();
    delete pDotSceneLoader;

    // Creamos el Mapa
    crearMapa();

    // Creamos el controlador de las camaras para que sigan a lucas
    _camerasController = new CamerasController(_sceneMgr, _camera, _camaraMapa, _lucas);
}

void PlayState::update(double timeSinceLastFrame)
{
    //    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    //    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::update");
    //    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    //    if ( _empieza_a_contar )
    _tiempo += timeSinceLastFrame;

    _camera->setAspectRatio(Ogre::Real(_viewport->getActualWidth()) / Ogre::Real(_viewport->getActualHeight()));

    // Fade in/out de los everlays
    for ( unsigned int i = 0; i < _vFader.size(); i++ )
        if (_vFader[i]) _vFader[i]->fade ( timeSinceLastFrame );

    //Comprobamos si hemos acabado el juego
    _jueboAcabado = _jueboAcabado || (_tiempoInicial - _tiempo) < 0 || _numElementos == 0;
    if(_jueboAcabado){
        //_exitGame = true;

        if((_tiempoInicial - _tiempo) >= 0 && !_muerto && _numElementos == 0){
            Records::getSingleton().anadirRecord(0, _tiempoInicial - _tiempo);
            Records::getSingleton().grabarRecords();
            changeState(WinningState::getSingletonPtr());
        }else{
            //cleanAndChangeState(GameOverState::getSingletonPtr());
            changeState(GameOverState::getSingletonPtr());
        }
    }else{

        _world->stepSimulation(timeSinceLastFrame); // Actualizar simulacion Bullet

        _lucas->pararMovimiento();
        // Movemos a Lucas
        if ( GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_RIGHT ) )
        {
            _lucas->girarDer();
        }
        if ( GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_LEFT ) )
        {
            _lucas->girarIzq();
        }
        if ( GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_DOWN ) )
        {
            _lucas->andar( - _velocidadLucas );
        }
        if ( GameManager::getSingletonPtr()->getInputMgrPtr()->getKeyboard()->isKeyDown ( OIS::KC_UP ) )
        {
            _lucas->andar ( _velocidadLucas );
        }
        _lucas->update(timeSinceLastFrame, _vPersonajes);

        // Actualizamos los enemigos
        if ( _enemigos.size() > 0 )
        {
            unsigned int i = 1;
            for ( std::deque<Enemy *>::iterator it = _enemigos.begin(); _enemigos.end() != it; it++, i++ )
            {
                (*it)->updateEnemy(timeSinceLastFrame, _vPersonajes, _lucas);
            }
        }

        // Actualizamos los elementos
        if ( _vElementos.size() > 0 )
        {
            unsigned int i = 1;
            for ( std::vector<Element *>::iterator it = _vElementos.begin(); _vElementos.end() != it; it++, i++ )
            {
                (*it)->update(timeSinceLastFrame, _vPersonajes);
            }
        }

        // comprobamos si ha cogido el elemento
        Element* elemento = detectarColisionElementos(_world, _lucas, _vElementos);
        if (elemento != NULL) {
            elemento->coger();
            _numElementos--;
        }

        detectarColision(_world, _lucas, _enemigos);

        // Actualizacion del overlay del tiempo
        Ogre::OverlayElement *elem = NULL;
        elem = _overlayMgr->getOverlayElement("txtTiempo");
        elem->setCaption ( getTime(_tiempoInicial -_tiempo) );

        // Actualizamos la cámara
        if (_camerasController != NULL)
            _camerasController->update(timeSinceLastFrame);
    }
}

//Para borrar
OgreBulletDynamics::RigidBody* PlayState::insertarElementoEscena (std::string nombreElemento, std::string mesh)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("PlayState::insertarElementoEscena");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    Ogre::Vector3 position = Ogre::Vector3 (0, 5, 0);

    Ogre::Entity *entity = _sceneMgr->createEntity(nombreElemento, mesh + std::string(".mesh"));
    Ogre::SceneNode *node = _sceneMgr->createSceneNode(nombreElemento);
    node->attachObject(entity);
    node->setPosition(position);

    _sceneMgr->getRootSceneNode()->addChild(node);
    OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = new OgreBulletCollisions::StaticMeshToShapeConverter(entity);

    //OgreBulletCollisions::TriangleMeshCollisionShape *trackTrimesh = trimeshConverter->createTrimesh();
    OgreBulletCollisions::ConvexHullCollisionShape *trackTrimesh = trimeshConverter->createConvex();


    OgreBulletDynamics::RigidBody *rigidTrack = new OgreBulletDynamics::RigidBody(nombreElemento, _world);
    rigidTrack->setShape(node, (OgreBulletCollisions::CollisionShape*) trackTrimesh, 0.8/* Restitucion */, 0.95/* Friccion */, 80.0f/* Masa */, position, Ogre::Quaternion::IDENTITY);
    rigidTrack->enableActiveState();

    delete trimeshConverter;
    return rigidTrack;
}

void PlayState::crearPlano ( Ogre::SceneManager* sceneMgr,
                             OgreBulletDynamics::DynamicsWorld* world,
                             Ogre::StaticGeometry *staticGeometry,
                             std::string namePlane,
                             std::string nameMaterial,
                             float width,
                             float height,
                             Ogre::Vector3 upVector,
                             Ogre::Vector3 normal,
                             const Ogre::Vector3& initial_pos ) {
    // Define a floor plane mesh
    Ogre::Entity *entPlane;
    Ogre::Plane plane;
    plane.normal = normal; plane.d = 0;
    Ogre::MeshManager::getSingleton().createPlane ( namePlane + "PlaneMesh",
                                                    Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                                                    plane, width, height,
                                                    1, 1, true, 1, 1, 1,
                                                    upVector );
    // Create an entity plane
    entPlane = sceneMgr->createEntity(namePlane  + "PlaneEntity", namePlane  + "PlaneMesh");
    entPlane->setMaterialName(nameMaterial);
    entPlane->setCastShadows(true);

    staticGeometry->addEntity(entPlane, initial_pos);

    // add collision detection to it
    OgreBulletCollisions::CollisionShape *shapePlane = new OgreBulletCollisions::StaticPlaneCollisionShape ( normal, 0 ); // (normal vector, distance)
    // a body is needed for the shape
    OgreBulletDynamics::RigidBody *defaultPlaneBody = new OgreBulletDynamics::RigidBody ( namePlane  + "PlaneBody", world );
    defaultPlaneBody->setStaticShape ( shapePlane, 0.1, 0.8, initial_pos);// (shape, restitution, friction, position)
}

void PlayState::crearEscenario()
{
    float widthScene = 50 * 2;
    float heightScene = 48 * 2;
    float heightWall = 10.0f;

    _staticGeometry = _sceneMgr->createStaticGeometry("StaticMap");

    // Creamos las paredes (Ponemos el material Menu/Vacio porque es el que tiene asignado el PNG de total transparencia)
    crearPlano(_sceneMgr, _world, _staticGeometry, "leftWall", "Menu/Vacio", heightScene, heightWall,
               Vector3::UNIT_Y, Vector3(1,0,0), Vector3(widthScene / -2.0f,0,0));
    crearPlano(_sceneMgr, _world, _staticGeometry, "rightWall", "Menu/Vacio", heightScene, heightWall,
               Vector3::UNIT_Y, Vector3(-1,0,0), Vector3(widthScene / 2.0f,0,0));
    crearPlano(_sceneMgr, _world, _staticGeometry, "upWall", "Menu/Vacio", widthScene, heightWall,
               Vector3::UNIT_Y, Vector3(0,0,1), Vector3(0,0,heightScene / -2.0f));
    crearPlano(_sceneMgr, _world, _staticGeometry, "downWall", "Menu/Vacio", widthScene, heightWall,
               Vector3::UNIT_Y, Vector3(0,0,-1), Vector3(0,0,heightScene / 2.0f));
    _staticGeometry->build();
}


string PlayState::getTime ( double time )
{
    unsigned int minutos = 0, segundos = 0;
    char cad[6];
    string ret = "";

    minutos = (int)time / 60;
    segundos = (int)time % 60;

    sprintf ( cad, "%02d:%02d", minutos, segundos );

    ret = cad;

    return ret;
}



Element* PlayState::detectarColisionElementos(OgreBulletDynamics::DynamicsWorld* world,
                                              Character* lucas,
                                              std::vector<Element*> personajes) {
    bool hayColision = false;
    Element* personajeColision = NULL;
    Element* personaje = NULL;

    btCollisionWorld *bulletWorld = world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    int numPersonajes = personajes.size();
    btPersistentManifold* contactManifold = NULL;
    btCollisionObject* obA;
    btCollisionObject* obB;

    // Recorremos las colisiones que se esten produciendo
    for (int i = 0 ; i < numManifolds && !hayColision ; i++)
    {
        contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
        obA = (btCollisionObject*)(contactManifold->getBody0());
        obB = (btCollisionObject*)(contactManifold->getBody1());

        OgreBulletCollisions::Object* obOB_A = world->findObject(obA);
        OgreBulletCollisions::Object* obOB_B = world->findObject(obB);

        if (obOB_A == lucas->getRigidBody() || obOB_B == lucas->getRigidBody()) {
            for (int j=0;j<numPersonajes && !hayColision;j++) {
                personaje = personajes[j];
                if (personaje->getState() == SIN_COGER ) {
                    Ogre::LogManager::getSingletonPtr()->logMessage("personaje->getEntity()->getName()");
                    Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::StringConverter::toString(_numElementos));
                    if(personaje->getEntity()->getName() != PLATAFORMA ||  personaje->getEntity()->getName()==PLATAFORMA && _numElementos==1){
                    hayColision = (obOB_A == personaje->getRigidBody() || obOB_B == personaje->getRigidBody());
                    if (hayColision) personajeColision = personaje;
                    }
                }
            }
        }
    }
    return personajeColision;
}

Character* PlayState::detectarColision(OgreBulletDynamics::DynamicsWorld* world,
                                       Character* lucas,
                                       std::deque<Enemy*> personajes) {

    bool hayColision = false;
    Character* personajeColision = NULL;
    Character* personaje = NULL;

    btCollisionWorld *bulletWorld = world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    int numPersonajes = personajes.size();
    btPersistentManifold* contactManifold = NULL;
    btCollisionObject* obA;
    btCollisionObject* obB;

    // Recorremos las colisiones que se esten produciendo
    for (int i = 0 ; i < numManifolds && !hayColision ; i++)
    {
        contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
        obA = (btCollisionObject*)(contactManifold->getBody0());
        obB = (btCollisionObject*)(contactManifold->getBody1());

        OgreBulletCollisions::Object* obOB_A = world->findObject(obA);
        OgreBulletCollisions::Object* obOB_B = world->findObject(obB);

        if (obOB_A == lucas->getRigidBody() || obOB_B == lucas->getRigidBody()) {

            //            int numContacts = contactManifold->getNumContacts();
            //            for (int j=0;j<numContacts;j++)
            //            {
            //                btManifoldPoint& pt = contactManifold->getContactPoint(j);
            //                if (pt.getDistance()<0.f){
            //                    personajeColision = personaje;
            //                    _jueboAcabado = true;
            //                    _muerto = true;
            //                    hayColision = true;
            //                }
            //            }

            for (int j=0;j<numPersonajes && !hayColision;j++) {
                personaje = personajes[j];
                hayColision = (obOB_A == personaje->getRigidBody() || obOB_B == personaje->getRigidBody());
                if (hayColision) {
                    personajeColision = personaje;
                    _jueboAcabado = true;
                    _muerto = true;
                }
            }
        }
    }
    return personajeColision;
}
