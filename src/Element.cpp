#include "Element.h"
#include "Constants.h"

Element::Element( Ogre::SceneManager* sceneMgr,
                  OgreBulletDynamics::DynamicsWorld* world,
                  const string& nombre,
                  const Ogre::Vector3& posicion ) : Character ( sceneMgr,
                                                                world,
                                                                nombre,
                                                                posicion,
                                                                ELEMENTO,
                                                                "" )
{
    Ogre::LogManager::getSingletonPtr()->logMessage("Constructor de elemento");

    _timerParticle = 0.0;

    //Material del Element
    _entityPuntoMapa->setMaterialName ( "MaterialAzul" );

    _state = SIN_COGER;

    // Creamos el sistema de partículas
    _particle = _sceneMgr->createParticleSystem("particle" + nombre, "Fairy");
    _particle->setVisible(false);
    // Creamos un nodo
    _particleNode = _node->createChildSceneNode("particleNode" + nombre);
    // Ajuntamos las partículas al nodo
    _particleNode->attachObject(_particle);

    _soundFreeFX = SoundFXManager::getSingleton().load(ELEMENTO_COGIDO_2);
}

Element::~Element()
{
    // Destruimos el nodo
    _sceneMgr->destroySceneNode(_particleNode);
    // Destruimos el sistema de partículas
    _sceneMgr->destroyParticleSystem(_particle);
}

Element::Element(const Element& personaje) : Character ( personaje )
{
    copy ( personaje );
}

Element& Element::operator=(const Element& rhs)
{
    if (this != &rhs)
        copy ( rhs );

    return *this;
}

void Element::copy ( const Element& personaje )
{
    _particle = personaje._particle;
    _particleNode = personaje._particleNode;
    _timerParticle = personaje._timerParticle;
}

void Element::changeAnimation(string nameAnimation) {

}

void Element::coger() {
    _state = COGIENDO;
    setVisible(false);
    _soundFreeFX->play();
}

void Element::setVisible ( const bool visible ) {
    Character::setVisible(visible);
    _particle->setVisible(!visible);
    if (visible) _state = COGIDO;
}

void Element::update ( double timeSinceLastFrame, std::vector<Character*> vCharacteres) {
    Character::update(timeSinceLastFrame, vCharacteres);
    flotar ();

    if (_state == COGIENDO) {
        _timerParticle += timeSinceLastFrame;
        _particle->setEmitting(true);
        if (_timerParticle > TIMER_PATICLE) {
            _particle->setEmitting(false);
            _state = COGIDO;
            //_stateCaracter = END;
        }
    } else {
        _timerParticle = 0.0;
    }
}

void Element::flotar ()
{
    if(getNombrePersonaje() == HUESO_NOMBRE){
        _rigidBody->enableActiveState();
        Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Y;

        orientacion.x = 0.0; // Movemos solo en el eje Y
        orientacion.z = 0.0; // Movemos solo en el eje Y

        if(getPosition().y > 5){
            orientacion = -Ogre::Vector3::UNIT_Y;
            _rigidBody->setLinearVelocity(orientacion * 2);
        }
        else if(getPosition().y < 3){
            orientacion = Ogre::Vector3::UNIT_Y;
            _rigidBody->setLinearVelocity(orientacion * 6);
        }
    }
}
