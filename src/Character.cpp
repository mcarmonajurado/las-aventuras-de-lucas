#include "Character.h"
#include "Constants.h"

#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h>

#include <OgreBullet/Dynamics/OgreBulletDynamicsWorld.h>
#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/Debug/OgreBulletCollisionsDebugDrawer.h>

#include <OgreBullet/Dynamics/Constraints/OgreBulletDynamicsRaycastVehicle.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsConvexHullShape.h>
#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include <OgreBullet/Collisions/OgreBulletCollisionsRay.h>

Character::Character ( Ogre::SceneManager* sceneMgr,
                       OgreBulletDynamics::DynamicsWorld* world,
                       const string& nombre,
                       const Ogre::Vector3& posicion,
                       TIPO_PERSONAJE tipo, string animacion ) : _nombrePersonaje(nombre), _sceneMgr(sceneMgr), _posicion(posicion)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("Constructor de personajes");

    // Inicialización del generador de numeros aleatorios
    srand(time(NULL));

    _tipoPersonaje = tipo;
    _node = NULL;
    _rigidBody = NULL;
    _nodePuntoMapa = NULL;
    _entityPuntoMapa = NULL;
    _visible = true;
    _world = world;
    _estadoPersonaje = ACTIVO;
    _distanciaAOtroPersonaje = 0.0;
    _vDistanciaAOtroPersonaje;
    _entity = NULL;

    string mesh =  LUCAS_MESH;

    if ( tipo == ENEMIGO )
    {
        mesh =  ENEMIGO_MESH;
    }
    else if (tipo == ELEMENTO)
    {
        mesh = ELEMENTO_MESH;
    }

    insertarEnEscena ( sceneMgr, world, mesh, nombre, posicion, &_entity, &_node, &_rigidBody, true, animacion);

    if (animacion == "")
    {
        _animacionActual = NULL;
    }
    else
    {
        _animacionActual = _entity->getAnimationState(animacion);
    }

    // Creamos el PuntoMapa para la vision y para el mini mapa
    Ogre::LogManager::getSingletonPtr()->logMessage("PuntoMapa");

    _entityPuntoMapa = sceneMgr->createEntity ( nombre + "PuntoMapa", CUBE_MESH + string ( EXTENSION_MESH ) );
    _entityPuntoMapa->setCastShadows(false);
    _entityPuntoMapa->setVisible(false);
    if ( tipo == ENEMIGO )
    {
        _entityPuntoMapa->setMaterialName ( "MaterialRojo" );
    }
    else if (tipo == ELEMENTO)
    {
        _entityPuntoMapa->setMaterialName ( "MaterialBlanco" );
    }

    _nodePuntoMapa = _node->createChildSceneNode ( nombre + "PuntoMapa" );
    _nodePuntoMapa->setPosition(0, (_entityPuntoMapa->getBoundingBox().getSize().y / -2), 0);
    _nodePuntoMapa->attachObject ( _entityPuntoMapa );

    // Cargamos el sonido
    //TODO
}

Character::~Character()
{
    _node->detachAllObjects();
    _sceneMgr->destroySceneNode(_node);
    _rigidBody = NULL;
    _sceneMgr->destroyEntity(_entity);
    _nodePuntoMapa->detachAllObjects();
    _sceneMgr->destroySceneNode(_nodePuntoMapa);
    _sceneMgr->destroyEntity(_entityPuntoMapa);
    _visible = true;
    _world = NULL;
}

Character::Character ( const Character& personaje )
{
    copy ( personaje );
}

Character& Character::operator=(const Character& rhs)
{
    if (this != &rhs)
        copy ( rhs );

    return *this;
}

void Character::copy ( const Character& personaje )
{
    Ogre::LogManager::getSingletonPtr()->logMessage("Character::copy");
    _nombrePersonaje = personaje.getNombrePersonaje();
    _tipoPersonaje = personaje.getTipoPersonaje();
    _posicion = personaje.getInitial_Pos();
    _node = personaje.getSceneNode();
    _rigidBody = personaje.getRigidBody();
    _animacionActual = personaje._animacionActual;
    _nodePuntoMapa = personaje._nodePuntoMapa;
    _entityPuntoMapa = personaje._entityPuntoMapa;
    _visible = personaje._visible;
    _world = personaje._world;
    _estadoPersonaje = personaje._estadoPersonaje;
    _distanciaAOtroPersonaje = personaje._distanciaAOtroPersonaje;
    _vDistanciaAOtroPersonaje = personaje._vDistanciaAOtroPersonaje;
}

void Character::cambiarAnimacion ( const string& nameAnimation )
{
    if ( ( _animacionActual != NULL ) &&
         ( _animacionActual->getAnimationName() != nameAnimation ) )
    {
        Ogre::AnimationState *animation;

        if ( PARAR_ANIMACION == nameAnimation )
        {
            animation = (_entity)->getAnimationState(INICIAR_ANIMACION);
            animation->setEnabled(false);
        }
        else if ( INICIAR_ANIMACION == nameAnimation )
        {
            animation = (_entity)->getAnimationState(PARAR_ANIMACION);
            animation->setEnabled(false);
        }

        animation = (_entity)->getAnimationState(nameAnimation);
        animation->setEnabled(true);
        animation->setLoop(true);
        _animacionActual = animation;
    }
}

void Character::update ( double timeSinceLastFrame, std::vector<Character*>   vPersonajes)
{
    if (_estadoPersonaje == INACTIVO) {
        //TODO
    } else {
        if ( _animacionActual != NULL )
        {
            _animacionActual->addTime(timeSinceLastFrame * VELOCIDAD_ANIMACION);
        }
    }
}

void Character::andar ( float velocidad )
{
    _rigidBody->enableActiveState();
    Ogre::Vector3 orientacion = _rigidBody->getCenterOfMassOrientation() * Ogre::Vector3::UNIT_Z;
    //orientacion.y = 0.0; // Movemos solo en los ejes Z y X
    _rigidBody->setLinearVelocity(orientacion * velocidad);

    cambiarAnimacion(INICIAR_ANIMACION);
}

void Character::pararMovimiento()
{
    _rigidBody->setLinearVelocity(Ogre::Vector3::ZERO);
    cambiarAnimacion(PARAR_ANIMACION);
}

void Character::girarIzq()
{
    moverAngulo ( Ogre::Radian ( Ogre::Math::HALF_PI / 32 ) );

    cambiarAnimacion(INICIAR_ANIMACION);
}

void Character::girarDer()
{
    moverAngulo ( Ogre::Radian ( (-1) * Ogre::Math::HALF_PI / 32 ) );

    cambiarAnimacion(INICIAR_ANIMACION);
}

void Character::moverAngulo ( const Ogre::Radian& angle )
{
    _rigidBody->enableActiveState();
    _node->yaw ( angle );
    btQuaternion quaternion = OgreBulletCollisions::OgreBtConverter::to ( _node->getOrientation() );
    _rigidBody->getBulletRigidBody()->getWorldTransform().setRotation ( quaternion );
}

void Character::showPuntoMapa(bool show) {
    //    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    //    Ogre::LogManager::getSingletonPtr()->logMessage("Character::showPuntoMapa");
    //    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    this->getEntity()->setVisible(!show);
    this->getEntityPuntoMapa()->setVisible(show);
}

void Character::setVisible ( const bool visible ) {
    _visible = visible;
    _node->setVisible(visible, true);

    if (visible) {
        _world->getBulletDynamicsWorld()->addCollisionObject(_rigidBody->getBulletObject());
    } else {
        _world->getBulletDynamicsWorld()->removeCollisionObject(_rigidBody->getBulletObject());
    }
}

const Ogre::Vector3& Character::getPosition()
{
    return _node->getPosition();
}

void Character::insertarEnEscena ( Ogre::SceneManager* sceneMgr,
                                   OgreBulletDynamics::DynamicsWorld* world,
                                   string mesh,
                                   string nombreElemento,
                                   const Ogre::Vector3& posicionInicial,
                                   Ogre::Entity** entity,
                                   Ogre::SceneNode** node,
                                   OgreBulletDynamics::RigidBody** rigidTrack,
                                   bool visible,
                                   string animacionInicial )
{
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
    Ogre::LogManager::getSingletonPtr()->logMessage("Character::insertarEnEscena");
    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");

    //    *entity = sceneMgr->createEntity ( nombreElemento, mesh + std::string ( ".mesh" ) );
    *entity = sceneMgr->getEntity(nombreElemento);
    (*entity)->setCastShadows(true);
    (*entity)->setVisible(visible);

    *node = sceneMgr->getSceneNode(nombreElemento);
    //    *node = sceneMgr->createSceneNode ( nombreElemento );
    //    (*node)->attachObject ( *entity );

    if (animacionInicial != "") {
        Ogre::AnimationState *animacion = (*entity)->getAnimationState(animacionInicial);
        animacion->setEnabled(true);
        animacion->setLoop(true);
    }

    //    sceneMgr->getRootSceneNode()->addChild ( *node );

    OgreBulletCollisions::CollisionShape* sceneBoxShape = NULL;

    if (animacionInicial == "") {
        OgreBulletCollisions::StaticMeshToShapeConverter* trimeshConverter = new
                OgreBulletCollisions::StaticMeshToShapeConverter ( *entity );

        sceneBoxShape = (OgreBulletCollisions::CollisionShape*) trimeshConverter->createConvex();

        delete trimeshConverter;
    } else {
        OgreBulletCollisions::AnimatedMeshToShapeConverter* trimeshConverter = new
                OgreBulletCollisions::AnimatedMeshToShapeConverter ( *entity );

        sceneBoxShape = (OgreBulletCollisions::CollisionShape*) trimeshConverter->createConvex();

        delete trimeshConverter;
    }

    *rigidTrack = new OgreBulletDynamics::RigidBody ( nombreElemento, world );

    (*rigidTrack)->setShape ( *node, sceneBoxShape, 0.6/* Restitucion */, 0.6/* Friccion */, 80.0f/* Masa */, posicionInicial, (*node)->_getDerivedOrientation() );
    (*rigidTrack)->getBulletRigidBody()->setAngularFactor(0.0f);
    (*rigidTrack)->enableActiveState();
}

double Character::distanciaAPersonaje(const Ogre::Vector3& position){
//    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");
//    Ogre::LogManager::getSingletonPtr()->logMessage("Character::distanciaAPersonaje");
//    Ogre::LogManager::getSingletonPtr()->logMessage("==============================");


    _distanciaAOtroPersonaje = getPosition().distance(position);
//    Ogre::LogManager::getSingletonPtr()->logMessage(_entity->getName());
//    Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::StringConverter::toString(getPosition().distance(position)));

    return _distanciaAOtroPersonaje;
}
