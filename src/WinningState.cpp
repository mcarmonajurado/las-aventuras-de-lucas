#include "WinningState.h"

#include "IntroState.h"
#include "Constants.h"

template<> WinningState* Ogre::Singleton<WinningState>::msSingleton = 0;

void
WinningState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager(SCENE_MANAGER);
    _camera = _sceneMgr->getCamera(CAMARA_PRINCIPAL);
//    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    // Nuevo background colour.
    _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 1.0, 1.0));

    _exitGame = false;

    buildGUI();

    _overlayMgr = Ogre::OverlayManager::getSingletonPtr();
    _overlayMgr->getByName ( "GUI_GameWin" )->show();
}

void
WinningState::exit ()
{
    if(!GameManager::getSingletonPtr()->getTrackPtr().isNull())
        GameManager::getSingletonPtr()->getTrackPtr()->stop();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _overlayMgr->getByName ( "GUI_GameWin" )->hide();

    if(GameManager::getSingletonPtr()->getTrayManager()){
        GameManager::getSingletonPtr()->getTrayManager()->clearAllTrays();
        GameManager::getSingletonPtr()->getTrayManager()->destroyAllWidgets();
        GameManager::getSingletonPtr()->getTrayManager()->setListener(0);
    }
}

void
WinningState::pause ()
{
    _overlayMgr->getByName ( "GUI_GameWin" )->hide();
}

void
WinningState::resume ()
{
}

bool
WinningState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
WinningState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

bool
WinningState::frameRenderingQueued
(const Ogre::FrameEvent& evt)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->frameRenderingQueued(evt);

    if (_exitGame)
        return false;

    return true;
}

void
WinningState::keyPressed
(const OIS::KeyEvent &e) {
    // Tecla p --> Estado anterior.
    if (e.key == OIS::KC_A) {
        popState();
    }
}

void
WinningState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
WinningState::mouseMoved
(const OIS::MouseEvent &e)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseMove(e);
}

void
WinningState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseDown(e, id);
}

void
WinningState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    if(GameManager::getSingletonPtr()->getTrayManager())
        GameManager::getSingletonPtr()->getTrayManager()->injectMouseUp(e, id);
}

WinningState*
WinningState::getSingletonPtr ()
{
    return msSingleton;
}

WinningState&
WinningState::getSingleton ()
{
    assert(msSingleton);
    return *msSingleton;
}

void WinningState::buildGUI()
{
    OgreBites::SdkTrayManager* trayMgr = GameManager::getSingletonPtr()->getTrayManager();
    trayMgr->setListener(this);
    trayMgr->destroyAllWidgets();
    trayMgr->showCursor();
    trayMgr->createLabel(OgreBites::TL_TOP, "WinningLbl", "¡Has ganado!", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "BackToMenuBtn", "Volver al Menu", 250);
    trayMgr->createButton(OgreBites::TL_CENTER, "ExitBtn", "Salir", 250);

    Ogre::FontManager::getSingleton().getByName("SdkTrays/Caption")->load();
    Ogre::FontManager::getSingleton().getByName("SdkTrays/Value")->load();
}

void WinningState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "ExitBtn")
        GameManager::getSingletonPtr()->getTrayManager()->showYesNoDialog("¿Estás seguro?", "¿Quieres salir realmente?");
    else if(button->getName() == "BackToMenuBtn")
        changeState(IntroState::getSingletonPtr());
}

void WinningState::yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit)
{
    if(yesHit == true)
        _exitGame = true;
    else
        GameManager::getSingletonPtr()->getTrayManager()->closeDialog();
}
